package sam.laidler

import org.apache.spark.SparkContext
import org.apache.log4j._

/**
 * Work out the average salary per age group
 */
object Bands extends App {
  def parse (str: String) = {
    val fields = str.split(",")
    val age = fields(2).toInt
    val salary = fields(3).toInt
    
    (age, salary)
  }
  
  Logger.getLogger("org").setLevel(Level.ERROR)
  
  val sc = new SparkContext("local[*]", "Bands")

  // id, name, age, salary
  val lines = sc.textFile("C:/PERSONAL PROJECTS/spark_bands/bands.csv")
  
  println("lines are as follows")
  lines.foreach(println)
  
  val ageSalary = lines.map(parse)
  println("Age, salary tuples:")
  ageSalary.foreach(println)
  
  val ageSalaryCounter = ageSalary.mapValues(x => (x, 1))
  println("Age, salary, counter tuples:")
  ageSalaryCounter.foreach(println)
  
  println("Total salaries per age group:")
  val totalSalariesPerAge = ageSalaryCounter.reduceByKey((x, y) => (x._1 + y._1, x._2 + y._2))
  totalSalariesPerAge.foreach(println)
  
  println("Average salaries per age group:")
  val averageSalariesPerAge = totalSalariesPerAge.mapValues((x) => (x._1 / x._2))
  averageSalariesPerAge.foreach(println)
  
  val results = averageSalariesPerAge.collect()
  
  println("Result:")
  results.sorted.foreach(println)
}